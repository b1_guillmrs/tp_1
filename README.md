# TP - 1
 
 # I. Self-footprinting

HOST - OS
---------------

```
$ PS C:\Users\guigu\YNOV_TP\project_1> systeminfo

Nom de l’hôte:                              GLAMA
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.19041 N/A version 19041
[...]
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : AMD64 Family 23 Model 24 Stepping 1 AuthenticAMD ~2100 MHz
[...]
Mémoire physique totale:                    7 104 Mo
[...]
```
Pour le nom des barettes de RAM on utilise une autre commande
```
$ PS C:\Users\guigu\YNOV_TP\project_1> Get-CimInstance win32_physicalmemory | Format-Table Manufacturer, PartNumber

Manufacturer PartNumber
------------ ----------
Samsung      M471A5244CB0-CTD
Samsung      M471A5244CB0-CTD
```

DEVICES
-----------------
Processeur: 
```
PS C:\Users\guigu\YNOV_TP\project_1> wmic
wmic:root\cli>cpu get Name,NumberOfCores
Name                                           NumberOfCores
AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx  4

```
Trackpad: 
```
PS C:\Users\guigu\YNOV_TP\project_1> Get-CimInstance Win32_PointingDevice | Format-Table Manufacturer, Caption

Manufacturer Caption
------------ -------
Microsoft    Souris HID
```
Carte Graphique : 
```
PS C:\Users\guigu\YNOV_TP\project_1> Get-CimInstance Win32_VideoController | Format-Table Caption

Caption
-------
AMD Radeon(TM) Vega 8 Graphics
```
```
PS C:\Users\guigu\YNOV_TP\project_1> Get-Disk

Number Friendly Name          Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                                      Style
------ -------------          -------------                    ------------         -----------------      ---------- ----------
0      WDC PC SN730 SDBPNT... E823_8FA6_BF53_0001_001B_448B... Healthy              Online                  476.94 GB GPT

```
Partitions
```
PS C:\Users\guigu\YNOV_TP\project_1> Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_wdc_pc_sn730_sdb#5&2bd20827&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                             Size Type
---------------  ----------- ------                                             ---- ----
1                           1048576                                          100 MB System #Démarrage
2                           105906176                                         16 MB Reserved
3                C           122683392                                         80 GB Basic #Stockage Windows + quelques fichiers
4                D           86022029312                                   381.33 GB Basic # stockage logiciels + fichiers
5                           495466840064                                     512 MB Recovery # Récupération
6                           496003710976                                      14 GB Recovery # Récupération
7                           511036096512                                       1 GB Recovery # Récupération
```
USERS
-----------------

```
PS C:\Users\guigu\YNOV_TP\project_1> Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
Glama              True
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Applicatio...
```

Le compte Full admin est "Administrateur"

NETWORK
-----------------
Carte réseau
```
PS C:\Users\GUILLAUME> Get-NetAdapter | fl Name, InterfaceIndex                                                         

Name           : Ethernet 2   #Carte Ethernet Physique
InterfaceIndex : 14

Name           : Ethernet     #Carte NordVPN
InterfaceIndex : 10

Name           : NordLynx     #Carte VPN
InterfaceIndex : 7

Name           : VirtualBox Host-Only Network #Carte Réseau VirtualBox
InterfaceIndex : 6

Name           : VirtualBox Host-Only Network #2 #Carte Réseau 2 VirtualBox
InterfaceIndex : 2
```
Ports ouverts 

```
PS C:\WINDOWS\system32> netstat -ab

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    192.168.1.102:49564    40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.102:49577    Chromecast:8008        ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49580    do-73:https            ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49588    Chromecast:8009        ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49604    wr-in-f188:5228        ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49607    52.113.199.78:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.102:49608    Google-Home-Mini:8009  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49615    52.113.199.39:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.102:49697    ec2-13-114-175-100:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49788    104.20.74.28:https     ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.102:49791    104.16.85.20:https     ESTABLISHED
 [chrome.exe]
PS C:\WINDOWS\system32>
```

# II. Scripting: 

Ici le script en PS1 développé sous Powershell IDE.

```
Write-Host " Créé par Guillaume Mareschal"
Write-Host " Développé le 10/11/2020"
Write-Host " Ce script affiche un résumé des informations de la machine"

Write-Host ""
Write-Host "Patientez pendant la récupération des informations... "

$system = Get-WmiObject win32_OperatingSystem
$totalPhysicalMem = $system.TotalVisibleMemorySize
$freePhysicalMem = $system.FreePhysicalMemory
$usedPhysicalMem = $totalPhysicalMem - $freePhysicalMem
$freeMemory = (systeminfo | Select-String "M‚moire physique disponible:").ToString().Split(':')[1].Trim()

Write-Host ""

Write-Host -NoNewLine "Nom de la machine: "
$system.CSName

Write-Host ""
Write-Host -NoNewLine "IP Principale: "
(((ipconfig | findstr [0-9].\.)[0]).Split()[-1])

Write-Host ""
Write-Host -NoNewLine "Nom de l'OS: "
$system.Caption

Write-Host ""
Write-Host -NoNewLine "Version de l'OS: "
$system.Version

Write-Host ""
Write-Host -NoNewLine "Date et heure allumage: "
(Get-CimInstance -ClassName win32_operatingsystem | Select-Object lastbootuptime | ForEach { $_.lastbootuptime })

Write-Host ""
Write-Host -NoNewLine "Espace RAM disponible: "
($freeMemory)

Write-Host ""
Write-Host -NoNewLine "Espace RAM utilisé (GO): "
($usedPhysicalMem / 1000000)

Write-Host ""
Write-Host -NoNewLine "Espace RAM disponible (GO): "
($system.TotalVisibleMemorySize / 1000000)

Write-Host ""
Write-Host "Utilisateurs locaux: "
Write-Host ""
(Get-LocalUser | Select-Object Name | ForEach { $_.Name })


pause
```
Ici la sortie en console : 
```
 Créé par Guillaume Mareschal
 Développé le 10/11/2020
 Ce script affiche un résumé des informations de la machine

Patientez pendant la récupération des informations...

Nom de la machine: PC-GUILLAUME

IP Principale: 192.168.1.102

Nom de l'OS: Microsoft Windows 10 Famille

Version de l'OS: 10.0.18362

Date et heure allumage:
mardi 10 novembre 2020 21:58:09

Espace RAM disponible: 8 483 Mo

Espace RAM utilisé (GO): 3,859536

Espace RAM disponible (GO): 12,547128

Utilisateurs locaux:

Administrateur
DefaultAccount
GUILLAUME
Invité
WDAGUtilityAccount
Cliquez sur Entrée pour continuer...:

```


# III. Gestion de softs: 

- L'intéret d'un gestionnaire de paquets par rapport au téléchargement direct est qu'il est plus sûr car vérifié donc il n'est pas censé y avoir de malwares ou autres et tout est centralisé sur une même plateforme pas besoin de parcourir internet pour trouver son fichier.

- L'éditeur de logiciel a une plateforme sûr ou déposer ses logiciels et nous on a une plateforme sûr ou télécharger ses paquets sans risque en théorie.


- Sécurisé car le fichier est vérifié et donc ne comporte pas de virus ou malware. Il est aussi mis a jour facilement donc évite les failles de sécurité.


Liste des paquets installés: 
```
PS C:\Users\GUILLAUME> choco list --local-only
Chocolatey v0.10.15

chocolatey 0.10.15
1 packages installed.

```

```
PS C:\Users\GUILLAUME> choco source
Chocolatey v0.10.15

chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```

Le serveur qui délivre les paquets est donc https://chocolatey.org/api/v2/ .

